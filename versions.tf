# versions.tf specifies minimum versions for providers

terraform {
  required_providers {
    docker = {
      # This is the "blessed" community docker provider.
      source  = "kreuzwerker/docker"
      version = "~> 3.0"
    }
    google = {
      source  = "hashicorp/google"
      version = "< 6.10"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 3.6"
    }
    null = {
      source  = "hashicorp/null"
      version = "~> 3.2"
    }
  }
  required_version = "~> 1.3"
}
