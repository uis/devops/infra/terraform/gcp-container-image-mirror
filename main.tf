# main.tf defines resources managed by the module

# Query the source image metadata from its registry.
data "docker_registry_image" "source" {
  name = var.source_image
}

# If var.pull_if_required is set, pull the container locally if there is a new
# version.
resource "docker_image" "source" {
  name         = local.source_image
  keep_locally = true
  platform     = var.platform

  pull_triggers = concat(
    var.pull_if_required ? [local.source_image] : []
  )
}

# We will make use of the Google Container registry. Ensure that an appropriate
# bucket has been created.
data "google_container_registry_repository" "default" {
  count = var.use_external_repository ? 0 : 1

  region  = var.gcr_region
  project = local.project
}

# Client credentials for the Google provider.
data "google_client_config" "default" {
}

# A random tag name for the pushed image.
resource "random_id" "tag" {
  byte_length = 8
  keepers     = local.combined_push_triggers # if we're pushing a new image, regenerate this id
  prefix      = var.tag_prefix
}

resource "null_resource" "gcr_image" {
  # Tag and push the image to the GCR.
  provisioner "local-exec" {
    command = <<EOI
      set -e
      docker tag "$SOURCE_IMAGE" "$DEST_IMAGE"
      echo $TOKEN | docker login -u oauth2accesstoken --password-stdin "https://$REPO_URL"
      docker push "$DEST_IMAGE"
EOI
    environment = {
      REPO_URL     = local.repository_url
      TOKEN        = data.google_client_config.default.access_token
      SOURCE_IMAGE = local.source_image
      DEST_IMAGE   = local.gcr_image
    }
  }

  # Trigger values which, when they change, will cause a push of the image.
  triggers = merge(
    local.combined_push_triggers,
    {
      # Use "__" as a prefix in an attempt to avoid collisions with
      # push_triggers.
      __destination_image = local.gcr_image
    }
  )

  depends_on = [
    docker_image.source,
  ]
}
