# Mirrored Google Container Registry image

A module which can mirror a named Docker image to Google Container Registry. By
default the image is always pulled if necessary although this can be disabled to
"pin" an image to the one held locally. If pulling is disabled, the image is
pushed when the source image *name* changes and so this can be used in
combination with SHA256 digests to ensure that a *specific* image is mirrored.

In order to uniquely refer to pushed images, this module tags images pushed to
GCR with a random tag. This tag is reflected in the image name provided by the
`image` output.

See [variables.tf](variables.tf) for all variable supported by this module.

## Requirements

This module requires that terraform be able to talk to the local docker daemon.
If using the
[logan](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/) utility,
remember to add `mount_docker_socket: true` to the logan configuration.

## Private repositories

In order to have access to images from private repositories in GitLab, you
will need to [create a deploy token for the repository](https://docs.gitlab.com/ee/user/project/deploy_tokens/index.html).
The deploy token only needs `read_registry` permissions.
Add the deploy token username and password to a secret in the deployment and
the following configuration to your `providers.tf`.

```tf
# providers.tf in root
provider "docker" {
  alias = "gitlab"

  registry_auth {
    address  = "registry.gitlab.developers.cam.ac.uk"
    username = local.gitlab_access_token_user
    password = data.google_secret_manager_secret_version.gitlab_access_token.secret_data
  }
}
```

and the following to your `versions.tf` file:

```tf
# versions.tf in root
terraform {
  required_providers {
    [...]

    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.15"
    }
  }
```

and use the provider in your code, e.g.:

```tf
module "echo_gcr_image" {
  source = "git::https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-container-image-mirror.git?ref=v2"

  # ...

  providers {
    docker = docker.gitlab
  }
}
```

For more information, see See https://registry.terraform.io/providers/kreuzwerker/docker/latest/docs

## Migrating to the artefact registry

As a stop-gap, you can continue using this module with the Google Artifact Registry by passing the
artifact registry URL created by the product factory in to the `external_repository_url` variable
and setting `use_external_repository` to `true`.

## Versioning

The current version of the module is in the `v1` branch and will remain
backwards compatible. If a major version update is required, this will be
maintained in a `v2` branch.

## Example

```tf
# Ensure that an upstream image is mirrored to Google Container Registry.
module "echo_gcr_image" {
  source = "git::https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-container-image-mirror.git?ref=v1"

  # Required: name of source image as it is known to the local docker daemon.
  # Use a SHA256 digest-style name if you want to precisely specify which image
  # should be mirrored.
  source_image = "ealen/echo-server:latest"

  # Required: name used in GCP. The GCP URL will be of the form
  # "[DEFAULT GCR REPOSITORY][NAME]:[RANDOM TAG]" and can be read via the
  # "image" output from the module.
  name = "third-party/echo-server"

  # Optional: project containing GCR. If omitted or blank the default project
  # set in the google provider will be used. Default: ""
  project = local.project

  # Optional: if true, pull the image if the version held locally is out of
  # date. If false, you must ensure that the source image exists locally by
  # other means. Default: true.
  pull_if_required = true

  # Optional: specify the tag prefix used for the mirrored image.
  # Default: "terraform-"
  tag_prefix = "upstream-"

  # Optional: specify the Google Container Registry region to mirror images to.
  # Default: "eu"
  gcr_region = "us"

  # Optional: arbitrary map from keys to values. If any value in the map changes
  # the image will be re-pushed to GCR. Default: {}
  push_triggers = {
    release_number = local.release_number
  }
}

# Deploy the mirrored image to Cloud Run.
resource "google_cloud_run_service" "echo" {
  name     = "echo"
  location = "europe-west2"

  template {
    spec {
      containers {
        image = module.echo_gcr_image.image
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
}
```
