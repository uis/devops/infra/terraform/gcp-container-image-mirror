# variables.tf defines variables used by the module

variable "project" {
  description = <<-EOL
    Google project whose container registry will be pushed to. If not specified,
    the Google provider's project is used.
EOL
  default     = ""
  type        = string
}

variable "tag_prefix" {
  description = <<-EOL
    Prefix for image tags used in GCR to uniquely refer to images.
EOL
  default     = "terraform-"
  type        = string
}

variable "pull_if_required" {
  description = <<-EOL
    Optional: if true, pull the image if the version held locally is out of
    date. If false, you must ensure that the source image exists locally by
    other means.
EOL
  default     = true
  type        = bool
}

variable "gcr_region" {
  description = <<-EOL
    Destination region for GCR image.
EOL
  default     = "eu"
  type        = string
}

variable "source_image" {
  description = <<-EOL
    Name of the upstream Docker image to pull, including any tags or SHA256 repo
    digests. Passed to the docker_registry_image resource.
EOL
  type        = string
}

variable "platform" {
  description = <<-EOL
    Platform of the image to pull. Passed to the docker_image resource. Defaulting
    to linux/amd64 as we are typically pushing to GCP for Cloud Run.
EOL
  default     = "linux/amd64"
  type        = string
}

variable "name" {
  description = <<-EOL
    Name of image as it will appear in the GCR. This should not include any tags
    or digests.
EOL
  type        = string
}

variable "push_triggers" {
  description = <<-EOL
    Arbitrary map of values. If any values change the image will be pushed to
    the GCR. Irrespective of this variable's value, the image will also always
    be pushed if the source image name or destination image name changes.
EOL
  default     = {}
  type        = map(string)
}

variable "external_repository_url" {
  description = <<-EOI
    Location of external repository to push image.

    If you are using the artifact registry, this will be something like:
    europe-west2-docker.pkg.dev/some-meta-project-id/default.

    Required if use_external_repository is true.
  EOI
  default     = null
  type        = string
}

variable "use_external_repository" {
  description = <<-EOI
    Set to true to use an external docker repository rather than the container registry. This is
    useful if you're trying to migrate from container registry to artifact registry.
  EOI
  default     = false
  type        = bool
}
