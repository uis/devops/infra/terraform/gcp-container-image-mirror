# outputs.tf specifies outputs from the module

output "image" {
  description = <<EOL
    Fully qualified name representing the pushed GCR image. This name refers
    specifically to the image pushed by the module.
EOL
  value       = local.gcr_image
  depends_on = [
    null_resource.gcr_image
  ]
}
