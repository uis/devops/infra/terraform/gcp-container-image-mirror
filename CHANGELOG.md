# Changelog

## [2.2.1](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-container-image-mirror/compare/2.2.0...2.2.1) (2025-03-05)

### Bug Fixes

* add platform to push triggers ([2725ba0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-container-image-mirror/commit/2725ba04106ed5fd700c21defa674d58ec94c7be))

## [2.2.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-container-image-mirror/compare/2.1.0...2.2.0) (2025-03-04)

### Features

* add platform var defaulting to amd64 ([cd9c95b](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-container-image-mirror/commit/cd9c95b5a41bfe591c5d3d6ad183a7f3078c4bb2))

## [2.1.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-container-image-mirror/compare/2.0.5...2.1.0) (2025-01-23)

### Features

* add migration support for artifact registry ([8f8a9d1](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-container-image-mirror/commit/8f8a9d13a0fcf29e3c267f261ff79596837d7f54))

## [2.0.5](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-container-image-mirror/compare/2.0.4...2.0.5) (2024-10-31)

## [2.0.4](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-container-image-mirror/compare/2.0.3...2.0.4) (2024-09-30)

## [2.0.3](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-container-image-mirror/compare/2.0.2...2.0.3) (2024-08-22)


### Bug Fixes

* merge two CHANGELOG files ([629a751](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-container-image-mirror/commit/629a751465d866e5f040aae140da4638f67f1ac0))

## [2.0.2](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-container-image-mirror/compare/2.0.1...2.0.2) (2024-08-22)
### Bug Fixes
* fix missing types in variables and missing required dependencies ([5370249](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-container-image-mirror/commit/537024923e0b0db5e3a879cb193a18f5733fc49c))
* local.project was never used ([b0ec749](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-container-image-mirror/commit/b0ec74926af8073591bfb099ca63f9ee6201056f))

## [2.0.1] - 2023-11-02
### Added
* Added support to publish to GitLab Terraform registry when tagged using semver

## [2.0.0] - 2022-05-23
### Changed
 - Allow a wider range of hashicorp/google versions.  Specifically, versions
   4.x and minor versions of 3.x later than 56 are now also permitted.
 - Restrict allowed terraform versions to exclude 2.x or greater, which have not
   yet been released.  This is to catch potentially breaking changes when 2.0 is released.

## [1.0.2] - 2021-02-26
### Changed
 - Explicitly use SHA256 checksums internally to avoid issues where local
   image tags differ from those in the source registry.

## [1.0.1] - 2021-02-15
### Changed
 - Made output dependent on image having been pushed.

## [1.0.0] - 2021-02-14
### Added
 - Intial version of this module
