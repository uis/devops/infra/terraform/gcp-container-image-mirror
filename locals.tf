# locals.tf defines local variables used throughout the module.

locals {
  # Use provider project if no override set.
  project = coalesce(var.project, data.google_client_config.default.project)

  # The source image is always referenced by its SHA256 to ensure it is
  # precisely the one corresponding to what the registry thinks the source
  # image is. To make sure of this we strip any tag name from the source and
  # use the SHA256 explicitly.
  source_image = "${replace(data.docker_registry_image.source.name, "/:[^:]*$/", "")}@${data.docker_registry_image.source.sha256_digest}"

  # The repository URL
  repository_url = var.use_external_repository ? var.external_repository_url : data.google_container_registry_repository.default[0].repository_url

  # The pushed image name without any tag
  untagged_gcr_image = "${local.repository_url}/${var.name}"

  # Form the pushed image URL for the default GCR repository.
  gcr_image = "${local.untagged_gcr_image}:${random_id.tag.hex}"

  # Combined push triggers. This merges custom push triggers specified in the
  # var.push_triggers variable and local triggers. Any changes to values in this
  # map will cause a new destination image tag to be created and pushed.
  #
  # We use the untagged image name here since the random tag itself is also
  # re-generated if these push triggers change.
  combined_push_triggers = merge(
    var.push_triggers,
    {
      # Use "__" as a prefix in an attempt to avoid collisions with
      # push_triggers.
      __source_image               = local.source_image
      __destination_image_untagged = local.untagged_gcr_image
      __platform                   = var.platform
    }
  )
}
